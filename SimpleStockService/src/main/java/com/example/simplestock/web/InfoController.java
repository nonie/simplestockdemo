package com.example.simplestock.web;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {
	
	@GetMapping("/info")
	@ResponseBody
	public String getInfo() throws UnknownHostException {
	    
		
		return InetAddress.getLocalHost().getHostName();
	}

}
