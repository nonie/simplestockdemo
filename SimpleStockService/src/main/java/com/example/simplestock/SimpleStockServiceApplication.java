package com.example.simplestock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleStockServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleStockServiceApplication.class, args);
	}

}
